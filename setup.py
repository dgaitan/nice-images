from setuptools import setup

required = []
with open('requirements.txt') as f:
    required = f.read().splitlines()

setup(
    name="nice_images",
    version="0.1",
    author="David Gaitan",
    author_email="jdavid.gaitan@gmail.com",
    install_requires=required,
    classifiers=[
        "Framework :: Django",
    ],
)
