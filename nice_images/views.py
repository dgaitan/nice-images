from django.conf import settings
from django.views.generic import View, TemplateView
from django.db.models import Q
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseForbidden
from django.http import JsonResponse
from django import forms
from django.core.paginator import Paginator

from .models import Image, Gallery, GalleryItem
from .forms import GalleryForm
from .utils import check_image


class BaseNiceView(View):

    def dispatch(self, request, *args, **kwargs):
        if self.request.user.is_staff and self.request.is_ajax():
            return super(BaseNiceView, self).dispatch(
                request, *args, **kwargs)
        else:
            return HttpResponseForbidden('Permiso Denegado')

    def default_error(self):
        return {'status': 'error'}

    def render_json_response(self, data):
        return JsonResponse(data)


class BaseNiceImageView(BaseNiceView):
    model = Image
    pk_param = 'pk'

    def delete(self, obj):
        result = obj.delete()
        if result:
            data = {
                'status': 'ok',
                'message': 'La imagen se ha eliminado'
            }
            self.render_json_response(data)

        return self.render_json_response({'status': 'error'})

    def get_image(self, pk):
        return self.model.objects.get(pk=pk)

    def get_images(self, ids):
        return self.model.objects.filter(pk__in=ids)


class LibraryView(LoginRequiredMixin, TemplateView):
    login_url = '/admin/login/?next=/admin/nice_images/library/'
    template_name = 'nice_images/admin/library.html'

    def get(self, request, *args, **kwargs):
        if self.request.user.is_staff:
            return super(LibraryView, self).get(request, *args, **kwargs)
        else:
            return HttpResponseForbidden('Permiso Denegado')


class ImageListView(BaseNiceImageView):

    def get(self, request, *args, **kwargs):
        NICE_IMAGES_PER_PAGE = 25
        per_page = getattr(settings, 'NICE_IMAGES_PER_PAGE', NICE_IMAGES_PER_PAGE) # noqa
        images_list = self.model.objects.all()
        pages = Paginator(images_list, per_page)
        page = self.request.GET.get('page')
        images = pages.page(page)
        images_json = []
        for i in images.object_list:
            images_json.append(i.get_json())

        data = {
            'status': 'ok',
            'pages': pages.num_pages,
            'images': images_json
        }

        return JsonResponse(data)


class ImageForm(forms.ModelForm):

    class Meta:
        model = Image
        fields = ('photo', )

    def clean_photo(self):
        formats = ['image/jpeg', 'image/png', 'image/jpg']
        photo = self.cleaned_data.get('photo')
        if photo:
            if photo.content_type not in formats:
                raise forms.ValidationError('The format of the image is wrong!') # noqa

            if photo.size > 8388608:
                raise forms.ValidationError('The size of the image is to big!')

        return photo


class ImageUpdateForm(forms.ModelForm):

    class Meta:
        model = Image
        fields = ('name', 'alt', 'caption', )


class ImageUpload(BaseNiceImageView):

    def post(self, request, *args, **kwargs):
        form = ImageForm(self.request.POST, self.request.FILES)
        if form.is_valid():
            photo = form.save(commit=False)
            photo.owner = self.request.user
            photo.save()
            photo.photo = check_image(photo.photo)
            photo.save()
            data = photo.get_json()
            data['status'] = 'ok'

        else:
            data = {
                'status': 'error',
                'errors': form.errors.get('photo')
            }

        return JsonResponse(data)


class ImageUpdate(BaseNiceImageView):

    def post(self, request, *args, **kwargs):
        qs = self.get_image(self.request.POST.get('pk'))
        form = ImageUpdateForm(self.request.POST, instance=qs)
        if form.is_valid():
            photo = form.save()
            data = photo.get_json()
            data['status'] = 'ok'
        else:
            data = {'status': 'error'}

        return JsonResponse(data)


class ImageDelete(BaseNiceImageView):

    def post(self, request, *args, **kwargs):
        qs = self.get_image(self.request.POST.get('pk'))
        return self.delete(qs)


class GetCurrentImage(BaseNiceImageView):

    def get(self, request):
        if self.request.GET.get('pk'):
            photo = self.get_image(self.request.GET.get('pk'))
            data = photo.get_json()
            data['status'] = 'ok'
        else:
            data = {'status': 'error'}

        return self.render_json_response(data)


class ImageSearch(View):

    def get(self, request):
        data = {'status': 'error'}
        if self.request.is_ajax():
            q = self.request.GET.get('q')
            if q:
                qs = Image.objects.filter(
                    Q(name__icontains=q) | Q(caption__icontains=q))
                images = []
                for photo in qs:
                    images.append(photo.get_json())

                data = {
                    'status': 'ok',
                    'images': images
                }

            return JsonResponse(data)
        return JsonResponse(data)


class ImageFilterView(BaseNiceImageView):

    def get(self, request, *args, **kwargs):
        if self.request.GET.get('ids'):
            ids = self.request.GET.get('ids').split(',')
            images = self.get_images(ids)
            images_list = []

            if images:
                for image in images:
                    images_list.append(image.get_json())

                data = {
                    'images': images_list,
                    'status': 'ok'
                }
            else:
                data = {
                    'status': 'not_found',
                    'message': 'images not found'
                }

            return self.render_json_response(data)
        else:
            return self.render_json_response({'status': 'error'})


class BaseNiceGalleryView(BaseNiceView):
    model = Gallery
    image_id = None
    gallery_id = None

    def post(self, request, *args, **kwargs):
        if self.request.POST.get('image_id'):
            self.image_id = self.request.POST.get('image_id')

        if self.request.POST.get('pk'):
            self.gallery_id = self.request.POST.get('pk')

    def get_gallery(self, pk):
        try:
            return self.model.objects.get(pk=pk)
        except self.model.DoesNotExist:
            return self.render_json_response({'status': '404'})

    def get_galleries(self):
        return self.model.objects.all()

    def delete_gallery_item(self):
        if self.image_id:
            obj = GalleryItem.objects.get(pk=self.image_id)
            obj.delete()
            return True
        return False


class GalleryCreateView(BaseNiceGalleryView):
    action = None
    form_class = GalleryForm

    def post(self, request, *args, **kwargs):
        self.action = self.request.POST.get('action', None)
        if self.action == 'create':
            form = self.form_class(self.request.POST)
        else:
            gallery = self.get_gallery(self.request.POST.get('pk'))
            form = self.form_class(self.request.POST, instance=gallery)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        gallery = form.save(commit=False)
        gallery.owner = self.request.user
        gallery.slug = self.request.POST.get('slug', None)
        gallery.save()
        data = {
            'status': 'ok',
            'gallery': gallery.get_json()
        }

        return self.render_json_response(data)

    def form_invalid(self, form):
        data = {
            'status': 'error',
            'errors': form.errors()
        }

        return self.render_json_response(data)


class GalleryDetailView(BaseNiceGalleryView):

    def get(self, request, *args, **kwargs):
        qs = self.get_gallery(self.request.GET.get('pk'))
        if qs:
            data = {
                'status': 'ok',
                'gallery': qs.get_json()
            }

            return self.render_json_response(data)

        return self.render_json_response(self.default_error())


class GalleryListView(BaseNiceGalleryView):

    def get(self, request, *args, **kwargs):
        qs = self.get_galleries()
        pages = Paginator(qs, 20)
        page = self.request.GET.get('page')
        galleries = pages.page(page)
        if qs:
            galleries_json = []
            for gallery in galleries.object_list:
                galleries_json.append(gallery.get_json())

            data = {
                'status': 'ok',
                'galleries': galleries_json,
                'pages': pages.num_pages
            }

            return self.render_json_response(data)

        return self.render_json_response(self.default_error())


class GalleryAddImages(BaseNiceGalleryView):

    def post(self, request, *args, **kwargs):
        image_id = self.request.POST.get('image_id')
        gallery_id = self.request.POST.get('pk')

        image = GalleryItem.objects.create(
            gallery_id=gallery_id, image_id=image_id, order=0)
        image.save()

        gallery = self.get_gallery(gallery_id)

        data = {
            'gallery': gallery.get_json(),
            'status': 'ok'
        }

        return self.render_json_response(data)


class GalleryDeleteImage(BaseNiceGalleryView):

    def post(self, request, *args, **kwargs):
        super(GalleryDeleteImage, self).post(request, *args, **kwargs)
        if self.delete_gallery_item():
            gallery = self.get_gallery(self.gallery_id)
            data = {
                'status': 'ok',
                'gallery': gallery.get_json()
            }

            return self.render_json_response(data)
        return self.render_json_response(self.default_error())
