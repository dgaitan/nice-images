from django.conf.urls import url

from .views import ImageUpload, GetCurrentImage, \
    ImageListView, ImageUpdate, ImageSearch, \
    ImageFilterView, ImageDelete, GalleryDetailView, \
    GalleryListView, GalleryAddImages, GalleryDeleteImage, \
    GalleryCreateView

urlpatterns = [
    url(r'^upload/images/$',
        view=ImageUpload.as_view(),
        name='upload_image'),
    url(r'^get_current/$',
        view=GetCurrentImage.as_view(),
        name='get_current'),
    url(r'^update/images/$',
        view=ImageUpdate.as_view(),
        name='update_image'),
    url(r'^search/$',
        view=ImageSearch.as_view(),
        name='image_search'),
    url(r'^get_images/$',
        view=ImageListView.as_view(),
        name='list_images'),
    url(r'^filter_images/$',
        view=ImageFilterView.as_view(),
        name='filter_images'),
    url(r'^delete/$',
        view=ImageDelete.as_view(),
        name='delete_image'),
    url(r'^gallery/detail/$',
        view=GalleryDetailView.as_view(),
        name='gallery_detail'),
    url(r'^gallery/all/$',
        view=GalleryListView.as_view(),
        name='gallery_list'),
    url(r'^gallery/add-images/$',
        view=GalleryAddImages.as_view(),
        name='gallery_add_images'),
    url(r'^gallery/delete_item/$',
        view=GalleryDeleteImage.as_view(),
        name='gallery_delete_item'),
    url(r'^gallery/create/$',
        view=GalleryCreateView.as_view(),
        name='gallery_create_view')
]
