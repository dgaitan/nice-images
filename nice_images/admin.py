from django.contrib import admin

from .models import Image, Gallery, GalleryItem


@admin.register(Image)
class ImageAdmin(admin.ModelAdmin):
    list_display = ['name', 'admin_thumbnail']
    list_per_page = 20


class GalleryItemInline(admin.TabularInline):
    model = GalleryItem
    raw_id_fields = ['image']


@admin.register(Gallery)
class GalleryAdmin(admin.ModelAdmin):
    inlines = [GalleryItemInline]
    list_display = ['name']
