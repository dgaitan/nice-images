from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

from autoslug import AutoSlugField

from .utils import GenericThumbModel


NICE_IMAGES_UPLOAD_TO = getattr(
    settings, 'NICE_IMAGES_UPLOAD_TO', 'library/images/')


class Image(GenericThumbModel, models.Model):
    name = models.CharField(
        _('Name'), max_length=500,
        blank=True, null=True)
    photo = models.ImageField(
        _('Photo'), upload_to=NICE_IMAGES_UPLOAD_TO)
    caption = models.TextField(
        _('Caption'), blank=True, null=True)
    alt = models.CharField(
        _('Alt'), max_length=500,
        blank=True, null=True)
    created_at = models.DateTimeField(
        _('Created At'), auto_now_add=True,
        blank=True, null=True)
    owner = models.ForeignKey(
        getattr(settings, 'AUTH_USER_MODEL', 'auth.User'),
        blank=True, null=True,
        related_name='nice_images_owner',
        on_delete=models.SET_NULL)

    class Meta:
        verbose_name = _('Image')
        verbose_name_plural = _('Images')
        ordering = ['-pk']

    def __str__(self):
        if self.name:
            return self.name
        else:
            return 'This is a Nice Image ;)'

    def save(self, *args, **kwargs):
        name = self.photo.name.split('/')[-1]
        if not self.name:
            self.name = name
        if not self.alt:
            self.alt = name
        if not self.caption:
            self.caption = name
        super(Image, self).save(*args, **kwargs)

    def get_sizes(self):
        return '{0}x{1}'.format(self.photo.width, self.photo.height)

    def get_file_name(self):
        return self.photo.name.split('/')[-1]

    def admin_thumbnail(self):
        return '<img src="{}" />'.format(self.thumb_150x150().url)

    def get_file_size(self):
        suffix = 'B'
        num = self.photo.size
        for unit in ['', 'K', 'M', 'G', 'T', 'P', 'E', 'Z']:
            if num < 1024.0:
                return "%3.1f%s%s" % (num, unit, suffix)
            num /= 1024.0
        return "%.1f%s%s" % (num, 'Yi', suffix)

    @property
    def url(self):
        return self.photo.url

    @property
    def width(self):
        return self.photo.width

    @property
    def height(self):
        return self.photo.height

    def get_json(self):
        return {
            'id': self.pk,
            'url': self.photo.url,
            'preview': self.thumb_250x250().url,
            'thumb': self.thumb_150x150().url,
            'width': self.photo.width,
            'height': self.photo.height,
            'name': self.name,
            'alt': self.alt if self.alt else '',
            'caption': self.caption if self.caption else '',
            'size': self.get_file_size(),
            'owner': self.owner.username if self.owner else '',
        }

    admin_thumbnail.allow_tags = True
    admin_thumbnail.short_description = _('Thumbnail')


class Gallery(models.Model):
    name = models.CharField(_('Name'), max_length=255)
    slug = AutoSlugField(populate_from='name')
    description = models.TextField(_('Description'), blank=True, null=True)
    created_at = models.DateTimeField(
        _('Created At'), auto_now_add=True,
        blank=True, null=True)
    owner = models.ForeignKey(
        getattr(settings, 'AUTH_USER_MODEL', 'auth.User'),
        blank=True, null=True,
        related_name='nice_images_galleries',
        on_delete=models.SET_NULL)

    class Meta:
        verbose_name = _('Gallery')
        verbose_name_plural = _('Galleries')

    def __str__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return ['gallery_detail', [self.pk]]

    def default_thumb(self):
        return '/static/nice_images/images/default-thumbnail.jpg'

    def get_images_json(self):
        images = []
        for i in self.gallery_images.all():
            images.append(i.get_json())

        return images

    def get_images(self):
        return self.gallery_images.all()

    def get_featured_image(self):
        try:
            image = self.gallery_images.first().image
            return {
                'thumb': image.thumb_150x150().url,
                'alt': image.alt
            }
        except Exception:
            return None

    def get_json(self):
        featured_image = self.get_featured_image()
        return {
            'id': self.pk,
            'name': self.name,
            'slug': self.slug,
            'description': self.description if self.description else '',
            'owner': self.owner.username,
            'images': self.get_images_json(),
            'featured_image': featured_image['thumb'] if featured_image else self.default_thumb(),  # noqa
            'featured_image_alt': featured_image['alt'] if featured_image else ''  # noqa
        }


class GalleryItem(models.Model):
    gallery = models.ForeignKey(Gallery, related_name='gallery_images')
    image = models.ForeignKey(Image)
    order = models.PositiveIntegerField()

    class Meta:
        verbose_name = _('Gallery Item')
        verbose_name_plural = _('Gallery Items')

    def __str__(self):
        return self.image.name

    def get_json(self):
        return {
            'id': self.pk,
            'image': self.image.get_json(),
            'order': self.order
        }
