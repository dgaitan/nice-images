from django.contrib.admin.widgets import ForeignKeyRawIdWidget, \
    ManyToManyRawIdWidget
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe
from django.utils.html import conditional_escape
from django.utils.encoding import force_text


class NiceImageWidget(ForeignKeyRawIdWidget):

    class Media:
        js = (
            'nice_images/react/build/static/js/main.js',
        )
        css = {
            'all': ['nice_images/css/nice.css']
        }

    def render(self, name, value, attrs=None):
        final_attrs = self.build_attrs(attrs)
        hidden_input = super(ForeignKeyRawIdWidget, self).render(
            name, value, attrs)

        return mark_safe(render_to_string('nice_images/widget.html', {
            'hidden_input': hidden_input,
            'final_attrs': final_attrs,
            'id': final_attrs['id'],
            'value': conditional_escape(force_text(value)),
        }))


class NiceImageGalleryWidget(ForeignKeyRawIdWidget):

    class Media:
        js = (
            'nice_images/react/build/static/js/main.js',
            # 'nice_images/js/nice_gallery.js',
        )
        css = {
            'all': ['nice_images/css/nice.css']
        }

    def render(self, name, value, attrs=None):
        final_attrs = self.build_attrs(attrs)
        hidden_input = super(ForeignKeyRawIdWidget, self).render(
            name, value, attrs)

        return mark_safe(render_to_string(
            'nice_images/widget_gallery.html', {
                'hidden_input': hidden_input,
                'final_attrs': final_attrs,
                'id': final_attrs['id'],
                'value': conditional_escape(force_text(value)),
            }))
