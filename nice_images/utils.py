from __future__ import absolute_import
import functools

from django.utils import six
from django.conf import settings

from sorl.thumbnail import get_thumbnail
from PIL import Image


def check_image(image):
    path = image.url
    path_list = path.split('.')
    full_path = '{0}{1}'.format(
        settings.MEDIA_ROOT.split('/media')[0], path)

    if 'png' in path_list:
        photo = Image.open(full_path)
        full_name = '{0}{1}_compressed.png'.format(
            settings.MEDIA_ROOT.split('/media')[0], path_list[0])
        image_name = '{}_compressed.png'.format(
            path_list[0].replace('/media/', '', 1))
        new_image = photo.convert('RGBA')
        new_image.save(full_name)
        return image_name

    return image


def get_model_label(model):
    """
    Helper from django-filer
    https://github.com/divio/django-filer
    """

    if isinstance(model, six.string_types):
        return model
    else:
        return "{0}.{1}".format(model._meta.app_label, model.__name__)


class GenericThumbModel(object):
    """
    Generates thumbs for given sizes
    Example:
        THUMBS_SIZES = ['220x208', '62x54', ...]
    Requirements:
        - sorl-thumbnail==11.12
    Usage:
        # In your models:
        class Article(GenericThumbModel, models.Model):
            photo = models.ImageField(upload_to='pics/')
            THUMBS_SIZES = [300x220, 200, x100]
            THUMBS_SOURCE_FIELD = 'photo'
        # In your views:
        article = Article.objects.get(pk=1)
        # In your templates:
        <img src="{{ article.thumb_320x220.url }}" alt="Thumb 300x220">
        <img src="{{ article.thumb_200.url }}" alt="Thumb width only 200px">
        <img src="{{ article.thumb_x100.url }}" alt="Thumb height only 100">
    Code by:
        Giacoman :p
        https://github.com/eos87
    """

    THUMBS_SIZES = ['150x150', '220x208', '600x400', '250x250']
    THUMBS_SOURCE_FIELD = 'photo'

    def __init__(self, *args, **kwargs):
        # model_id = '{0}.{1}'.format(
        #     self._meta.app_label, self._meta.model_name)
        # thumbs_sizes = getattr(
        #     settings, self.THUMBS_SIZES).get(
        #     model_id, self.THUMBS_SIZES)
        thumbs_sizes = getattr(settings, 'THUMBS_SIZES', self.THUMBS_SIZES)

        if '150x150' not in thumbs_sizes:
            thumbs_sizes.append('150x150')

        if '250x250' not in thumbs_sizes:
            thumbs_sizes.append('250x250')

        for key in thumbs_sizes:
            setattr(self, 'thumb_%s' %
                    key, functools.partial(self._THUMB, key))
        return super(GenericThumbModel, self).__init__(*args, **kwargs)

    def _THUMB(self, key):
        return get_thumbnail(
            getattr(self, self.THUMBS_SOURCE_FIELD), key, crop='top')
