from django.db import models
from django import forms
from django.contrib.admin.sites import site

from .models import Image, Gallery
from .utils import get_model_label
from .widgets import NiceImageWidget, NiceImageGalleryWidget

RECURSIVE_RELATIONSHIP_CONSTANT = 'self'


class NiceImageFormField(forms.ModelChoiceField):
    widget = NiceImageWidget

    def __init__(self, rel, queryset, to_field_name, *args, **kwargs):
        self.rel = rel
        self.queryset = queryset
        self.to_field_name = to_field_name
        self.max_value = None
        self.min_value = None
        kwargs.pop('widget', None)

        super(NiceImageFormField, self).__init__(
            queryset, widget=self.widget(rel, site), *args, **kwargs)


class NiceImageField(models.ForeignKey):
    default_model_class = get_model_label(Image)
    default_form_class = NiceImageFormField

    def __init__(self, **kwargs):
        kwargs['to'] = self.default_model_class
        kwargs['on_delete'] = models.SET_NULL

        if 'blank' not in kwargs:
            kwargs['blank'] = True
        if 'null' not in kwargs:
            kwargs['null'] = True

        super(NiceImageField, self).__init__(**kwargs)

    def formfield(self, **kwargs):
        defaults = {
            'form_class': self.default_form_class,
            'rel': self.rel
        }

        defaults.update(kwargs)
        return super(NiceImageField, self).formfield(**defaults)


class NiceImageGalleryFormField(forms.ModelChoiceField):
    widget = NiceImageGalleryWidget

    def __init__(self, rel, queryset, to_field_name, *args, **kwargs):
        self.rel = rel
        self.queryset = queryset
        self.to_field_name = to_field_name
        self.max_value = None
        self.min_value = None
        kwargs.pop('widget', None)

        super(NiceImageGalleryFormField, self).__init__(
            queryset, widget=self.widget(rel, site), *args, **kwargs)


class NiceImageGalleryField(models.ForeignKey):
    default_model_class = get_model_label(Gallery)
    default_form_class = NiceImageGalleryFormField

    def __init__(self, **kwargs):
        kwargs['to'] = self.default_model_class
        kwargs['on_delete'] = models.SET_NULL

        if 'blank' not in kwargs:
            kwargs['blank'] = True
        if 'null' not in kwargs:
            kwargs['null'] = True

        super(NiceImageGalleryField, self).__init__(**kwargs)

    def formfield(self, **kwargs):
        defaults = {
            'form_class': self.default_form_class,
            'rel': self.rel
        }

        defaults.update(kwargs)
        return super(NiceImageGalleryField, self).formfield(**defaults)
