export function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

export function searchAndDelete(json, index, value) {
    for ( var i in json ) {
        if ( json[i][index] === value ) {
            delete json[i];
        }
    }
}

export function slugify(text) {
    return text.toString().toLowerCase()
        .replace(/\s+/g, '-')           // Replace spaces with -
        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
        .replace(/\-\-+/g, '-')         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, '');            // Trim - from end of text
}

const urlNamespace = 'nice_images/';

export const urls = {
    image_list: urlNamespace + 'get_images/',
    image_upload: urlNamespace + 'upload/image/',
    image_detail: urlNamespace + 'get_current/',
    image_update: urlNamespace + 'update/images/',
    image_delete: urlNamespace + 'delete/'
};