import React from 'react';
import $ from 'jquery';
import { getCookie } from './utils';

class ImageDetail extends React.Component {

    getMainClass() {
        if ( this.props.is_active ) {
            return 'ni-browser__detail active';
        } else {
            return 'ni-browser__detail';
        }
    }

    render() {
        return (
            <div className={this.getMainClass()}>
                <div className="ni-browser__detail-wrapper">
                    <div className="ni-browser__detail-header">
                        <h5>{this.props.image.name}</h5>
                        <button type="button" className="ni-button ni-button-small" onClick={this.props.close}>cerrar</button>
                    </div>
                    <div className="ni-browser__detail-main">
                        <div className="ni-browser__detail-image">
                            <img src={this.props.image.url} alt={this.props.image.alt} />
                        </div>
                        <div className="ni-browser__detail-edits">
                            <ul className="ni-list">
                                <li><strong>Nombre del archivo: </strong> {this.props.image.name}</li>
                                <li><strong>URL: </strong> {this.props.image.url}</li>
                                <li><strong>Dimensions:</strong> {this.props.image.width}x{this.props.image.height}</li>
                                <li><strong>File Size:</strong> {this.props.image.size}</li>
                                <li><strong>Created by:</strong> {this.props.image.owner}</li>
                            </ul>
                            <hr />
                            <form className="ni-form" onSubmit={this.props.onSubmit}>
                                <div className="ni-form-group">
                                    <label>Nombre del archivo:</label>
                                    <input type="text" name="name" className="ni-input" placeholder="Nombre" value={this.props.values.name} onChange={this.props.onChange} />
                                </div>
                                <div className="ni-form-group">
                                    <label>Alt:</label>
                                    <input type="text" name="alt" className="ni-input" placeholder="Alt" value={this.props.values.alt} onChange={this.props.onChange} />
                                </div>
                                <div className="ni-form-group">
                                    <label>Caption:</label>
                                    <textarea name="caption" value={this.props.values.caption} className="ni-input" placeholder="Caption" onChange={this.props.onChange}></textarea>
                                </div>
                                <div className="ni-form-group">
                                    <button type="submit" className="ni-button">Guardar</button>
                                    <button type="button" className="ni-button danger" onClick={this.props.handleDelete}>Eliminar</button>
                                </div>
                                <div className="ni-form-group">
                                    <label>{this.props.message}</label>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


export default class Library extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            page_title: 'Library',
            images: [],
            current_image: {},
            showing_detail: false,
            form_values: {},
            search: '',
            current_page: 1,
            pages: 10,
            image_message: ''
        };

        this.handleDetailMobile = this.handleDetailMobile.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleUploadImage = this.handleUploadImage.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handlePagination = this.handlePagination.bind(this);
    }

    getImagesAPI() {
        var self = this;
        $.ajax({
            type: 'GET',
            url: '/nice_images/get_images/',
            data: { page: self.state.current_page },
            success: function(data){
                self.setState({
                    images: data.images,
                    pages: data.pages,
                });
            }
        });
    }

    componentDidMount() {
        return this.getImagesAPI();
    }

    showDetail(id) {
        var self = this;
        $.ajax({
            type: 'GET',
            url: '/nice_images/get_current/',
            data: { pk: id },
            success: function(data){
                self.setState({
                    current_image: data,
                    form_values: data,
                    showing_detail: true
                });
            }
        });
    }

    handleSearch(e) {
        let self = this;
        self.setState({search: e.target.value});

        if ( e.target.value.length > 3 ) {
            self.setState({current_page: 1});
            $.ajax({
                type: 'GET',
                url: '/nice_images/search/',
                data: { q: self.state.search },
                success: function(data){
                    self.setState({ images: data.images });
                }
            });
        } else {
            return self.getImagesAPI();
        }
    }

    handlePagination() {
        let page = this.state.current_page + 1; 
        this.setState({current_page: page});
        let self = this;
        $.ajax({
            type: 'GET',
            url: '/nice_images/get_images/',
            data: { page: page },
            success: function(data){
                let images = self.state.images;
                let new_images = images.concat(data.images);
                self.setState({
                    images: new_images,
                });
            }
        });
    }

    getImages() {
        if (this.state.images.length > 0) {
            return this.state.images.map((image) =>
                <li key={image.id} role="checkbox" aria-checked="true">
                    <img src={image.thumb} alt={image.alt} className="ni-browser__image" data-id={image.id} onClick={this.showDetail.bind(this, image.id)} />
                </li>
            );
        } else {
            return 'No se encontraron imagenes';
        }
    }

    handleDetailMobile() {
        this.setState({
            showing_detail: false,
            current_image: {},
            form_values: {},
            image_message: '',
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        var self = this;
        $.ajax({
            type: 'POST',
            url: '/nice_images/update/images/',
            data: {
                pk: self.state.current_image.id,
                csrfmiddlewaretoken: getCookie('csrftoken'),
                name: self.state.form_values.name,
                alt: self.state.form_values.alt,
                caption: self.state.form_values.caption
            },
            success: function(data) {
                self.setState({
                    current_image: data,
                    form_values: data,
                    image_message: 'Guardado!'
                });
            }
        });
    }

    handleInputChange(e) {
        let form_values = Object.assign({}, this.state.form_values);
        form_values[e.target.name] = e.target.value
        this.setState({form_values});
        this.setState({image_message: ''});
    }

    handleUploadImage() {
        let self = this;
        let file = $('#ni-button-upload');
        file.click();

        file.fileupload({
            dataType: 'json',
            maxChunkSize: 10000000,
            formData: {'csrfmiddlewaretoken': getCookie('csrftoken')},
            done: function (e, data) {
                self.setState({
                    showing_detail: true,
                    form_values: data.result,
                    current_image: data.result
                });
                self.getImagesAPI();
            },
        });
    }

    handleDelete() {
        let self = this;
        $.ajax({
            type: 'POST',
            url: '/nice_images/delete/',
            data: { 
                pk: self.state.current_image.id,
                csrfmiddlewaretoken: getCookie('csrftoken'),
            },
            success: function(data) {
                self.setState({
                    showing_detail: false,
                });
                self.getImagesAPI();
            }
        });
    }

    renderPaginator() {
        if ( this.state.current_page < this.state.pages ) {
            return <button type="button" className="ni-button" onClick={this.handlePagination}>Cargar mas imagenes</button>
        }
    }

    render() {
        return (
            <div className="ni-browser">
                <div className="ni-browser__header">
                    <h4>{this.state.page_title}</h4>
                    <button type="button" className="ni-button" onClick={this.handleUploadImage}>Subir Imagen</button>
                    <input type="file" id="ni-button-upload" name="photo" className="hidden" data-url="/nice_images/upload/images/" />
                </div>
                <div className="ni-browser__toolbar">
                    <input type="search" className="ni-input" name="s" onKeyUp={this.handleSearch} placeholder="Buscar imagen..." />
                </div>
                <div className="ni-browser__images">
                    <ul className="ni-browser__images-list">
                        {this.getImages()}
                    </ul>
                    {this.renderPaginator()}
                </div>
                <ImageDetail image={this.state.current_image} is_active={this.state.showing_detail} close={this.handleDetailMobile} onSubmit={this.handleSubmit} values={this.state.form_values} onChange={this.handleInputChange} handleDelete={this.handleDelete} message={this.state.image_message} />
            </div>
        )
    }
}