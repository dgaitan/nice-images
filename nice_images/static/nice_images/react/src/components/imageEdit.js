import React from 'react';
import $ from 'jquery';
import { getCookie } from '../utils';


export default class ImageEdit extends React.Component {
    /*
    * This Component is for update an image
    * This Component require the image object for update like <ImageEdit image={image} />
    * And require the toggle tho active/desactive the modal like:
    * <ImageEdit image={image} modal_active={modal_active} />
    * 
    * In resumen: Params required:
    * image: {}, type object
    * modal_active: false/true, type Boolean
    */
    constructor(props){
        super(props);
        this.state = {
            modal_active: false,
            image: props.image,
            values: {
                ni_name: props.image.name,
                ni_alt: props.image.alt,
                ni_caption: props.image.caption
            },
            message: ''
        }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    // Local Handles
    handleSubmit() {
        let self = this;
        $.ajax({
            type: 'POST',
            url: '/nice_images/update/images/',
            data: {
                pk: self.state.image.id,
                csrfmiddlewaretoken: getCookie('csrftoken'),
                name: self.state.values.ni_name,
                alt: self.state.values.ni_alt,
                caption: self.state.values.ni_caption
            },
            success: function(data) {
                self.setState({
                    image: data,
                    message: 'Guardado!'
                });
            }
        });
    }

    handleChange(e) {
        let values = Object.assign({}, this.state.values);
        values[e.target.name] = e.target.value
        this.setState({values});
        this.setState({message: ''});
    }

    getMainClass() {
        if ( this.props.modal_active ) {
            return 'ni-browser__detail active';
        } else {
            return 'ni-browser__detail';
        }
    }

    render() {
        let image = this.state.image;
        return (
            <div className={this.getMainClass()}>
                <div className="ni-browser__detail-wrapper">
                    <div className="ni-browser__detail-header">
                        <h5>{image.name}</h5>
                        <button type="button" className="ni-button ni-button-small" onClick={this.props.handleClose}>Cerrar</button>
                    </div>
                    <div className="ni-browser__detail-main">
                        <div className="ni-browser__detail-image">
                            <img src={image.url} alt={image.alt} />
                        </div>
                        <div className="ni-browser__detail-edits">
                            <ul className="ni-list">
                                <li><strong>Nombre del archivo: </strong> {image.name}</li>
                                <li><strong>URL: </strong> {image.url}</li>
                                <li><strong>Dimensions:</strong> {image.width}x{image.height}</li>
                                <li><strong>File Size:</strong> {image.size}</li>
                                <li><strong>Created by:</strong> {image.owner}</li>
                            </ul>
                            <hr />
                            <div className="ni-form">
                                <div className="ni-form-group">
                                    <label>Nombre del archivo:</label>
                                    <input type="text" name="ni_name" className="ni-input" placeholder="Nombre" value={this.state.values.ni_name} onChange={this.handleChange} />
                                </div>
                                <div className="ni-form-group">
                                    <label>Alt:</label>
                                    <input type="text" name="ni_alt" className="ni-input" placeholder="Alt" value={this.state.values.ni_alt} onChange={this.handleChange} />
                                </div>
                                <div className="ni-form-group">
                                    <label>Caption:</label>
                                    <textarea name="ni_caption" value={this.state.values.ni_caption} className="ni-input" placeholder="Caption" onChange={this.handleChange}></textarea>
                                </div>
                                <div className="ni-form-group">
                                    <button type="button" className="ni-button" onClick={this.handleSubmit}>Guardar</button>
                                </div>
                                <div className="ni-form-group">
                                    <label>{this.state.message}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}