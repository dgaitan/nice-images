import React from 'react';
import GalleryForm from './galleryCreate';
import ImageLibrary from './imageLibrary';


export default class Gallery extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            modal_active: false,
            image_library: false
        };

        this.handleEdit = this.handleEdit.bind(this);
        this.handleGalleryData = this.handleGalleryData.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
        this.handleImageLibrary = this.handleImageLibrary.bind(this);
        this.handleCloseImageLibrary = this.handleCloseImageLibrary.bind(this);
        this.handleSelectedImages = this.handleSelectedImages.bind(this);
    }

    handleGalleryData(gallery) {
        this.props.handleUpdate(gallery);
    }

    handleImageDelete(id) {
        this.props.handleImageDelete(id);
    }

    handleEdit(){
        this.setState({ modal_active: true });
    }

    handleCloseModal() {
        this.setState({ modal_active: false });
    }

    handleImageLibrary() {
        this.setState({image_library: true});
    }

    handleCloseImageLibrary() {
        this.setState({image_library: false});
    }

    handleSelectedImages(images) {

    }

    getImages() {
        if ( this.props.gallery.images ) {
            return this.props.gallery.images.map((image) =>
                <div key={image.id} className="ni-gallery__images-item">
                    <div className="ni-gallery__image">
                        <div className="ni-gallery__actions">
                            <button type="button" className="ni-button ni-button-small" onClick={this.handleImageDelete.bind(this, image.id)}>Eliminar</button>
                        </div>
                        <img src={image.image.thumb} alt={image.image.alt} />
                    </div>
                </div>
            )
        } else {
            return 'Esta galeria todavía no tiene imagenes.'
        }
    }

    render() {
        return(
            <div>
                <div className="ni-gallery">
                    <h4 className="ni-gallery__title">{this.props.gallery.name}</h4>
                    <button type="button" className="ni-button ni-button-small ni-button-to-right" onClick={this.handleEdit}>Editar</button>
                    <div className="ni-gallery__images">
                        {this.getImages()}
                        <div className="ni-gallery__images-item">
                            <div className="ni-gallery__image ni-gallery__add">
                                <span onClick={this.props.handleAddImageToGallery}>Agregar Imagen</span>
                            </div>
                        </div>
                    </div>
                </div>
                <GalleryForm modal_active={this.state.modal_active} gallery={this.props.gallery} action='edit' galleryData={this.handleGalleryData} handleCloseForm={this.handleCloseModal} />
            </div>
        )
    }
}