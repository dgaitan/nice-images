import React from 'react';
import $ from 'jquery';
import { getCookie, slugify } from '../utils';
import BaseModal from '../core/BaseModal';


export default class GalleryForm extends BaseModal {

    constructor(props) {
        super(props);
        this.state = {
            values: {
                ni_name: '',
                ni_slug: '',
                ni_description: '',
                owner: ''
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        if ( this.props.action === 'edit' ) {
            let gallery = this.props.gallery;
            let values = {
                ni_name: gallery.name,
                ni_slug: gallery.slug,
                ni_description: gallery.description
            };

            this.setState({ values: values });
        }
    }

    handleChange(e){
        let values = Object.assign({}, this.state.values);
        if ( e.target.name === 'name' ) {
            // this is a hook for slugify the slug... plop xD
            values[e.target.name] = e.target.value;
            values['ni_slug'] = slugify(e.target.value);
        } else {
            values[e.target.name] = e.target.value;
        }

        this.setState({values});
    }

    handleSubmit(){
        let self = this;
        $.ajax({
            type: 'POST',
            url: '/nice_images/gallery/create/',
            data: {
                pk: (self.props.action === 'edit') ? self.props.gallery.id : '',
                action: self.props.action,
                name: self.state.values.ni_name,
                slug: self.state.values.ni_slug,
                description: self.state.values.ni_description,
                csrfmiddlewaretoken: getCookie('csrftoken'),
            },
            success: function (data) {
                self.props.galleryData(data.gallery);
            }
        });
    }

    getGalleryInfo(){
        if ( this.props.action === 'edit' ) {
            let gallery = this.props.gallery;
            return (
                <ul className="ni-list">
                    <li><strong>Nombre: </strong> {gallery.name}</li>
                    <li><strong>URL: </strong> {gallery.slug}</li>
                    <li><strong>Created by:</strong> {gallery.owner}</li>
                    <li><strong>Descripcion: </strong> {gallery.description}</li>
                </ul>
            );
        }
    }

    render() {
        return(
            <div className={this.getMainClass()}>
                <div className="ni-browser__detail-wrapper gallery-edit">
                    <div className="ni-browser__detail-header">
                        <h5>{this.getHeaderText()}</h5>
                        <button type="button" className="ni-button ni-button-small" onClick={this.props.handleCloseForm}>Cerrar</button>
                    </div>
                    <div className="ni-browser__detail-main">
                        <div className="ni-browser__detail-edits">
                            {this.getGalleryInfo()}
                            <hr />
                            <div className="ni-form">
                                <div className="ni-form-group">
                                    <label>Nombre:</label>
                                    <input type="text" name="ni_name" className="ni-input" placeholder="Nombre" value={this.state.values.ni_name} onChange={this.handleChange} />
                                </div>
                                <div className="ni-form-group">
                                    <label>Slug:</label>
                                    <input type="text" name="ni_slug" className="ni-input" placeholder="Slug" value={this.state.values.ni_slug} onChange={this.handleChange} readonly />
                                </div>
                                <div className="ni-form-group">
                                    <label>Descripcion:</label>
                                    <textarea name="ni_description" value={this.state.values.ni_description} className="ni-input" placeholder="Descripcion" onChange={this.handleChange}></textarea>
                                </div>
                                <div className="ni-form-group">
                                    <button type="button" className="ni-button" onClick={this.handleSubmit}>Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
