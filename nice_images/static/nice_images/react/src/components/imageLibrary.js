import React from 'react';
import $ from 'jquery';
import { urls, searchAndDelete } from '../utils';


export default class ImageLibrary extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            modal_active: false,
            images: [],
            images_to_add: [],
            current_page: 1,
            pages: 10
        }

        this.handlePagination = this.handlePagination.bind(this);
    }

    componentDidMount(){
        let self = this;
        $.ajax({
            type: 'GET',
            url: '/nice_images/get_images/',
            data: { page: self.state.current_page },
            success: function(data) {
                self.setState({
                    images: data.images,
                    pages: data.pages
                });
            }
        });
    }

    handlePagination() {
        let page = this.state.current_page + 1; 
        this.setState({current_page: page});
        let self = this;
        $.ajax({
            type: 'GET',
            url: '/nice_images/get_images/',
            data: { page: page },
            success: function(data){
                let images = self.state.images;
                let new_images = images.concat(data.images);
                self.setState({
                    images: new_images,
                });
            }
        });
    }

    getModalMainClass() {
        if ( this.props.modal_active ) {
            return 'ni-modal active';
        } else {
            return 'ni-modal';
        }
    }

    handleSelectImage(image){
        if ( this.props.multiple === 'true' ) {
            let images = this.state.images_to_add;
            images.push(image);
            this.setState({images_to_add: images});
        } else {
            this.props.handleSelectImage(image);
        }
    }

    handleUnSelectImage(image) {
        let images = this.state.images_to_add;
        searchAndDelete(images, 'id', image.id);
        this.setState({images_to_add: images});
    }

    imageItemClass() {
        if ( this.props.multiple === 'true' ) {
            return 'ni-modal__list-item multiple';
        } else {
            return 'ni-modal__list-item';
        }
    }

    renderImages() {
        return this.state.images.map((image) =>
            <li key={image.id} className={this.imageItemClass()} >
                <figure>
                    <img src={image.thumb} alt={image.alt} onClick={this.handleSelectImage.bind(this, image)} />
                </figure>
            </li>
        );
    }

    renderPaginator() {
        if ( this.state.current_page < this.state.pages ) {
            return <button type="button" className="ni-button" onClick={this.handlePagination}>Cargar mas imagenes</button>
        }
    }

    render() {
        return(
            <div className={this.getModalMainClass()}>
                <div className="ni-modal__wrapper">
                    <div className="ni-modal__header">
                        <div className="ni-modal__header-left">
                            <h3 className="ni-modal__title">Fotos</h3>
                        </div>
                        <div className="ni-modal__header-right">
                            <button type="button" className="ni-close-modal" onClick={this.props.handleCloseModal}>
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    <div className="ni-modal__content">
                        <ul className="ni-modal__list">
                            {this.renderImages()}
                        </ul>
                        {this.renderPaginator()}
                    </div>
                </div>
            </div>
        );
    }
}