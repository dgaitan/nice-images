import React from 'react';
import ImageEdit from './imageEdit';


export default class Image extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            image: props.image,
            modal_active: false
        };

        this.handleEdit = this.handleEdit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }

    // local Methods
    handleEdit() {
        this.setState({modal_active: true});
    }

    handleDelete() {
        this.props.handleDelete();
    }

    handleClose() {
        this.setState({
            modal_active: false
        });
    }

    // views

    render() {
        return(
            <div className="ni-image">
                <div className="ni-image__actions">
                    <button type="button" className="ni-button ni-button-small" onClick={this.handleEdit}>Editar</button>
                    <button type="button" className="ni-button ni-button-small" onClick={this.handleDelete}>Eliminar</button>
                </div>
                <img src={this.props.image.preview} alt={this.props.image.alt} />
                <ImageEdit image={this.props.image} modal_active={this.state.modal_active} handleClose={this.handleClose} />
            </div>
        );
    }
}