import React from 'react';
import $ from 'jquery';


export default class GalleryLibrary extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            galleries: [],
            urls: props.urls,
            modal_active: props.modal_active,
            gallery: {},
            current_page: 1,
            pages: 10,
        };

        this.handlePagination = this.handlePagination.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
    }

    getGalleries() {
        let self = this;
        if ( this.props.reloadData ) {
            $.ajax({
                type: 'GET',
                url: self.state.urls.list,
                data: { page: self.state.current_page },
                success: function(data) {
                    self.setState({
                        galleries: data.galleries,
                        pages: data.pages
                    });
                }
            });
            return this.props.offReloadData();
        }
    }

    componentDidMount() {
        this.getGalleries();
    }

    handleSelectGallery(gallery) {
        this.setState({current_page: 1});
        this.props.handleSelectGallery(gallery);
    }

    handleCloseModal() {
        this.setState({current_page: 1});
        this.props.handleCloseModal();
    }

    handlePagination(){
        let page = this.state.current_page + 1; 
        this.setState({current_page: page});
        let self = this;
        $.ajax({
            type: 'GET',
            url: self.state.urls.list,
            data: { page: page },
            success: function(data) {
                let galleries = self.state.galleries;
                let new_galleries = galleries.concat(data.galleries);
                self.setState({
                    galleries: new_galleries,
                    pages: data.pages
                });
            }
        });
    }

    getModalMainClass() {
        if ( this.props.modal_active ) {
            return 'ni-modal active';
        } else {
            return 'ni-modal';
        }
    }


    renderGalleries() {
        this.getGalleries();
        if ( this.state.galleries ) {
            return this.state.galleries.map((gallery) =>
                <li key={gallery.id} className="ni-modal__list-item" onClick={this.handleSelectGallery.bind(this, gallery)}>
                    <figure>
                        <img src={gallery.featured_image} alt={gallery.featured_image_alt} />
                        <figcaption>{gallery.name}</figcaption>
                    </figure>
                </li>
            );
        } else {
            return 'No se encontraron galerías';
        }
    }
    
    renderPaginator() {
        if ( this.state.current_page < this.state.pages ) {
            return <button type="button" className="ni-button" onClick={this.handlePagination}>Cargar mas imagenes</button>
        }
    }

    render() {
        return (
            <div className={this.getModalMainClass()}>
                <div className="ni-modal__wrapper">
                    <div className="ni-modal__header">
                        <div className="ni-modal__header-left">
                            <h3 className="ni-modal__title">Galerías</h3>
                        </div>
                        <div className="ni-modal__header-right">
                            <button type="button" className="ni-close-modal" onClick={this.handleCloseModal}>
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    <div className="ni-modal__content">
                        <ul className="ni-modal__list">
                            {this.renderGalleries()}
                        </ul>
                        {this.renderPaginator()}
                    </div>
                </div>
            </div>
        )
    }
}