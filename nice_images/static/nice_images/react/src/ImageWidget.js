import React from 'react';
import $ from 'jquery';
import ImageLibrary from './components/imageLibrary';
import Image from './components/image';


export default class ImageWidget extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            current_image: {},
            status: 1,
            modal_active: false,
            messages: []
        };

        // bind local handle Methods
        this.handleUpload = this.handleUpload.bind(this);
        this.handleShowLibrary = this.handleShowLibrary.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
        this.handleSelectImage = this.handleSelectImage.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    // init the component
    initWidget(){
        let self = this;
        if ( this.props.current_image_id !== "" ) {
            $.ajax({
                type: 'GET',
                url: this.props.urls.detail,
                data: { pk: this.props.current_image_id },
                success: function(image){
                    self.setState({ current_image: image, status: 2 });
                }
            });
        }
    }

    // React Methods
    componentDidMount() {
        this.initWidget();
    }

    // Local handles 
    handleUpload() {
        let uploader = $('#ni-image-uploader-' + this.props.prefix);
        let self = this;
        uploader.click();

        uploader.fileupload({
            dataType: 'json',
            formData: {'csrfmiddlewaretoken': $('input[name=csrfmiddlewaretoken]').val()},
            maxChunkSize: 10000000,
            progressall: function (e, data) {
                let progress = parseInt(data.loaded / data.total * 100, 10);
                let strProgress = progress + "%";
                $(".ni-progress").addClass('active');
                $(".ni-progress-bar").css({"width": strProgress});
            },
            done: function (e, data) {
                if ( data.result.status === 'ok' ) {
                    $('#' + self.props.prefix).val(data.result.id);
                    self.setState({
                        current_image: data.result,
                        status: 2,
                        messages: []
                    });
                } else {
                    self.setState({
                        messages: data.result.errors
                    });
                }
            }
        }).on('fileuploadchunkfail', function(e, data) {
            self.setState({message: 'Los archivos pesan mas de 10MB'});
        });
    }

    handleShowLibrary() {
        this.setState({modal_active: true});
        $('body').addClass('overflow-hidden');
    }

    handleCloseModal() {
        this.setState({modal_active: false});
        $('body').removeClass('overflow-hidden');
    }

    handleSelectImage(image) {
        $('#' + this.props.prefix).val(image.id);
        this.setState({
            current_image: image,
            modal_active: false,
            status: 2
        });
        $('body').removeClass('overflow-hidden');
    }

    handleDelete() {
        this.setState({
            current_image: {},
            status: 1
        });

        $('#' + this.props.prefix).val('');
    }

    // Component Methods
    showCurrentImage() {
        if ( this.state.status === 2 ) {
            return <Image image={this.state.current_image} handleDelete={this.handleDelete} />
        }
    }

    renderMessages() {
        if ( this.state.messages.length ) {
            return this.state.messages.map((error) =>
                <p>{error}</p>
            );
        }
    }

    // React render
    render(){
        return (
            <div>
                <div className="ni-space">
                    <button type="button" className="ni-button" onClick={this.handleUpload}>Subir Imagen</button>
                    <button type="button" className="ni-button" onClick={this.handleShowLibrary}>Ver Biblioteca</button>
                </div>
                {this.showCurrentImage()}
                <div className="ni-messages">
                    {this.renderMessages()}
                </div>
                <div className="ni-progress">
                    <div className="ni-progress-bar"></div>
                </div>
                <ImageLibrary modal_active={this.state.modal_active} handleCloseModal={this.handleCloseModal} handleSelectImage={this.handleSelectImage} multiple="false" />
            </div>
        );
    }
}
