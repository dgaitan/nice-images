import React from 'react';
import $ from 'jquery';
import GalleryLibrary from './components/galleryLibrary.js';
import Gallery from './components/gallery';
import GalleryForm from './components/galleryCreate';


export default class GalleryWidget extends React.Component {
    /*
    * The GalleryWidget Component is the container for the widget
    * of Nice Image. This is check if the RawIdHiddenInput of django
    * have a id of gallery selected and show the gallery.
    *
    * The states of this component:
    *   1. current_gallery_id (string): this take the current id of the RawHiddenInput of Django
    *   2. current_gallery (object): this save the current gallery to manipulate.
    *   3. urls (object): this have the urls to can save, update, create a gallery.
    *   4. modal_active (booelan): this active/desactive the gallery modal to can select a gallery already created.
    *   5. create_gallery (boolean): this active/desactive the modal to create a gallery
    *
    * The Methods:
    *   1. show(): this check if isset a gallery selected and get the gallery it!
    *   2. renderGallery(): this check if isset a gallery selected and show it!
    *   3. addGalleryItem(): this add a new item to the gallery
    *
    * The Handles:
    *   1. handleCreate(): this active the modal with the component <GalleryForm /> to can create a gallery
    *   2. handleShowLibrary(): this active the modal with the component <GalleryLibrary /> to show the galleries created before!
    *   3. handleAddImageToGallery(): this handle upload and save the new images added to the gallery
    *   4. handleImageDelete(): this delete an image from the gallery
    *   5. handleSelectGallery(): this select a gallery from the modal library
    *   6. handleCloseModal(): this close the modal of the component <GalleryLibrary />
    *   7. handleGalleryDataForEdit(): this get the gallery created with the component <GalleryForm /> and pass the gallery to the current_gallery state. 
    */

    // constructor stuff
    constructor(props) {
        super(props);

        // set the states
        this.state = {
            current_gallery_id: props.current_gallery,
            current_gallery: {},
            urls: props.urls,
            status: 1,
            modal_active: false,
            ids_to_add: [],
            create_gallery: false,
            reload_data: false
        }

        // bind the events for this component
        this.handleCreate = this.handleCreate.bind(this);
        this.handleShowLibrary = this.handleShowLibrary.bind(this);

        // bind event for Gallery Component
        this.handleAddImageToGallery = this.handleAddImageToGallery.bind(this);
        this.handleImageDelete = this.handleImageDelete.bind(this);
        this.handleUpdateGallery = this.handleUpdateGallery.bind(this);

        // this handle is used for the gallery library
        this.handleSelectGallery = this.handleSelectGallery.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);

        // this methods if for gallery formData
        this.handleGalleryDataForEdit = this.handleGalleryDataForEdit.bind(this);
        this.handleCloseCreateModal = this.handleCloseCreateModal.bind(this);
        this.handleOffReloadData = this.handleOffReloadData.bind(this);
    }

    // initial method

    show() {
        // this method get the current imagen in case of exist
        let self = this;
        if ( self.props.current_gallery !== "" ) {
            $.ajax({
                type: 'GET',
                url: self.props.urls.detail,
                data: { pk: parseInt(self.props.current_gallery) },
                success: function(data) {
                    self.setState({
                        current_gallery: data.gallery,
                        status: 2
                    });
                }
            });
        }
    }

    // React Methods

    componentDidMount() {
        this.show();
    }

    // Methods of this component

    /*
    *   Add an item to the gallery
    *   Params:
    *       id (integer): the id of the image to can get the image.
    *       self (object): the instance of the component
    */ 
    addGallerItem(id, self){
        $.ajax({
            type: 'POST',
            url: this.props.urls.add_images,
            data: {
                pk: this.state.current_gallery.id,
                image_id: id,
                csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
            },
            success: function(data) {
                self.setState({ current_gallery: data.gallery });
            }
        });
    }

    // here will the handles methods

    handleCreate() {
        this.setState({create_gallery: true});
    }

    handleShowLibrary() {
        this.setState({
            modal_active: true,
            reload_data: true
        }); 
    }

    handleCloseModal() {
        this.setState({modal_active: false});
    }

    handleAddImageToGallery() {
        let uploader = $('#ni-gallery-images-' + this.props.prefix);
        let self = this;
        uploader.click();

        uploader.fileupload({
            dataType: 'json',
            sequentialUploads: true,
            formData: {'csrfmiddlewaretoken': $('input[name=csrfmiddlewaretoken]').val()},
            done: function (e, data) {
                self.addGallerItem(data.result.id, self);
            },
        });

    }

    /*
    *   Delete an image from the gallery
    *   Params:
    *       id (integer): the id of the gallery item and not of the image.
    */
    handleImageDelete(id) {
        // this is not the best way for do it, but in the moment
        // of do it, I was fuckin ungry... 
        // If you're (to my alter ego) have time to fix it, please just do it
        let self = this;
        $.ajax({
            type: 'POST',
            url: self.props.urls.delete_item,
            data: {
                pk: self.state.current_gallery.id,
                image_id: id,
                csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
            },
            success: function ( data ) {
                self.setState({
                    current_gallery: data.gallery
                });
            }
        });
    }

    /*
    *   Select the gallery object from the <GalleryLibrary /> component and
    *   save it on the state.
    *
    *   Params:
    *       gallery (object): the gallery object.
    */
    handleSelectGallery(gallery) {
        this.setState({
            current_gallery: gallery,
            modal_active: false,
            status: 2,
        });

        $('#' + this.props.prefix).val(gallery.id);
    }

    /*
    *   Get the gallery created from the <GalleryForm /> component
    *   and pass it to the current_gallery state.
    *
    *   Params:
    *       gallery (object): the gallery object created!
    */
    handleGalleryDataForEdit(gallery){
        $('#' + this.props.prefix).val(gallery.id);
        this.setState({
            current_gallery: gallery,
            create_gallery: false,
            status: 2,
            reload_data: true
        });
    }

    handleOffReloadData() {
        this.setState({reload_data: false});
    }

    /*
    *   Get the gallery updated from the <Gallery /> component
    *   and pass it to the current_gallery state.
    *
    *   Params:
    *       gallery (object): the gallery object updated!
    */
    handleUpdateGallery(gallery) {
        this.setState({ current_gallery: gallery });
    }

    handleCloseCreateModal() {
        this.setState({create_gallery: false});
    }

    renderGallery() {
        if ( this.state.status === 1 ) {
            return ''
        } else {
            return <Gallery gallery={this.state.current_gallery} handleAddImageToGallery={this.handleAddImageToGallery} handleImageDelete={this.handleImageDelete} handleUpdate={this.handleUpdateGallery} />
        }
    }

    render() {
        return (
            <div>
                <div className="ni-space">
                    <button type="button" className="ni-button" onClick={this.handleCreate}>Crear</button>
                    <button type="button" className="ni-button" onClick={this.handleShowLibrary}>Ver Galerias</button>
                </div>
                {this.renderGallery()}
                <GalleryLibrary urls={this.state.urls} modal_active={this.state.modal_active} handleSelectGallery={this.handleSelectGallery} handleCloseModal={this.handleCloseModal} reloadData={this.state.reload_data} offReloadData={this.handleOffReloadData}/>
                <GalleryForm modal_active={this.state.create_gallery} galleryData={this.handleGalleryDataForEdit} action='create' handleCloseForm={this.handleCloseCreateModal} />
            </div>
        )
    }
}
