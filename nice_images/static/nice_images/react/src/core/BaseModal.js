import React from 'react';


export default class BaseModal extends React.Component {
    constructor(props){
        super(props);
    }

    getMainClass() {
        if ( this.props.modal_active ) {
            return 'ni-browser__detail active';
        } else {
            return 'ni-browser__detail';
        }
    }

    getHeaderText() {
        if ( this.props.action === 'edit' ) {
            return this.props.gallery.name;
        } else {
            return 'Creando Galeria';
        }
    }
}
