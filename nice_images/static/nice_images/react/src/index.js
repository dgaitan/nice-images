import React from 'react';
import ReactDOM from 'react-dom';
import $ from 'jquery';
import fileupload from 'blueimp-file-upload';
import GalleryWidget from './GalleryWidget';
import ImageWidget from './ImageWidget';
import Library from './Library';


$(document).ready(function(){

    // render gallery widget if isset a field on the forms :p
    // jquery is loaded from cdn

    if ( $('.ni-gallery-field').length ) {
        $('.ni-gallery-field').each(function(){
            var self = $(this);
            var component_id = 'ni-gallery-' + self.data('field');
            var prefix = self.data('field');
            var current_gallery = $('#' + self.data('field')).val();
            var api_urls = {
                detail: self.data('url-detail'),
                list: self.data('url-all'),
                add_images: self.data('url-add-images'),
                delete_item: self.data('url-delete-item')
            };

            ReactDOM.render(
                <GalleryWidget prefix={prefix} current_gallery={current_gallery} urls={api_urls} />,
                document.getElementById(component_id));
            self.parents('.related-widget-wrapper').find('.related-widget-wrapper-link').hide();
        });
    }


    // load the image widget if isset on the forms
    if ( $('.ni-image-field').length ) {
        $('.ni-image-field').each(function(){
            var self = $(this);
            var component_id = 'ni-image-component-' + self.data('field');
            var prefix = self.data('field');
            var current_image_id = $('#' + self.data('field')).val();
            var api_urls = {
                detail: self.data('url-detail'),
                all: self.data('url-all')
            };

            ReactDOM.render(
                <ImageWidget prefix={prefix} current_image_id={current_image_id} urls={api_urls} />,
                document.getElementById(component_id));
            self.parents('.related-widget-wrapper').find('.related-widget-wrapper-link').hide();
        });
    }

    if ( $('#nice_images_library').length ) {
        ReactDOM.render(
            <Library />,
            document.getElementById('nice_images_library')
        );
    }

    if ( $('.ni-modal__list-item.multiple').length ) {
        $('.ni-modal__list-item.multiple').on('click', function(){
            $(this).toggleClass('selected');
        });
    }
});
