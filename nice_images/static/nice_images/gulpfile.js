
////////////////////////////////
    //Setup//
////////////////////////////////

// Plugins
var gulp = require('gulp'),
      pjson = require('./package.json'),
      gutil = require('gulp-util'),
      concat = require('gulp-concat'),
      sass = require('gulp-sass'),
      autoprefixer = require('gulp-autoprefixer'),
      cssnano = require('gulp-cssnano'),
      rename = require('gulp-rename'),
      plumber = require('gulp-plumber'),
      pixrem = require('gulp-pixrem'),
      uglify = require('gulp-uglify');

// Relative paths function
var pathsConfig = function () {
  this.app = "./";

  return {
    app: this.app,
    css: this.app + 'css',
    sass: this.app + 'sass',
  }
};

var paths = pathsConfig();

////////////////////////////////
    //Tasks//
////////////////////////////////

// Styles autoprefixing and minification
gulp.task('styles', function() {
  return gulp.src(paths.sass + '/nice.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(plumber()) // Checks for errors
    .pipe(autoprefixer({browsers: ['last 3 version', '> 5%', 'ie > 8', 'ios > 5']})) // Adds vendor prefixes
    .pipe(gulp.dest(paths.css))
    .pipe(rename({ suffix: '.min' }))
    .pipe(cssnano()) // Minifies the result
    .pipe(gulp.dest(paths.css));
});

// Default task
gulp.task('default', function() {
    gulp.watch(paths.sass + '/**/*.scss', ['styles']);
});
