$(document).ready(function(){
    // init the modal
    $('body').prepend('<div class="nice_images_fake_bg"></div>');

    $('.nice_images_open_modal').on('click', function(){
        var self = $(this);
        var modal_id = self.data('modal-id');
        var layout = $('#nice_images_modal_photos-' + modal_id);

        $.ajax({
            type: 'GET',
            url: '/nice_images/get_images/',
            success: function(data){
                console.log(data);
                //debugger;
                var source = $('#nice_images_library_photos_template-' + modal_id).html();
                var template = Handlebars.compile(source);
                layout.html(template(data));
            }
        });

        $('#nice_images_modal-' + modal_id).addClass('active');
        $('.nice_images_fake_bg').addClass('active');

    });

    $('.nice_button_close').on('click', function(){
        $('.nice_images_modal').removeClass('active');
        $('.nice_images_fake_bg').removeClass('active'); 
    });

    // this is for single fields
    var nice_image_wrapper = $('.nice_images_field_wrapper');
    var nice_image_trigger = $('.nice_images_trigger');

    nice_image_wrapper.each(function(){
        var self = $(this);
        self.parent().find('.related-widget-wrapper-link').hide();
        var id = self.data('field-id');
        var current = $('#' + id);
        if ( current.val().length > 0 ) {
            $.ajax({
                type: 'GET',
                url: '/nice_images/get_current/',
                data: { pk: parseInt(current.val()) },
                success: function(data){
                    // debugger;
                    var layout = $('#nice_images_field_layout-' + id);
                    var source = $('#nice_images_content_template-' + id).html();
                    var template = Handlebars.compile(source);
                    layout.html(template(data));
                }
            });
        }
    });
    
    nice_image_trigger.click(function () {
        var wrapper = $(this).parent();
        var key = wrapper.data('field-id');
        var field = $('#nice_images-' + key);
        var hidden_field = $('#' + key);
        field.click();
        
        field.fileupload({
            dataType: 'json',
            maxChunkSize: 10000000,
            formData: {'csrfmiddlewaretoken': $('input[name=csrfmiddlewaretoken]').val()},
            done: function (e, data) {
                hidden_field.val(data.result.id);
                var layout = $('#nice_images_field_layout-' + key);
                var source = $('#nice_images_content_template-' + key).html();
                var template = Handlebars.compile(source);
                layout.html(template(data.result));
            },
            progressall: function (e, data) {
                //debugger;
                var progress = parseInt(data.loaded / data.total * 100, 10);
                var strProgress = progress + "%";
                wrapper.find(".nice_images_process_bar").css({"width": strProgress});
                wrapper.find(".nice_images_process_bar").text(strProgress);
            },
        });
    });

    function niceModalSelectImage(){
        return alert('nice');
    }
});

function setImagePreview(field_id, image_id) {
    $.ajax({
        type: 'GET',
        url: '/nice_images/get_current/',
        data: { pk: parseInt(parseInt(image_id)) },
        success: function(data){
            // debugger;
            var layout = $('#nice_images_field_layout-' + field_id);
            var source = $('#nice_images_content_template-' + field_id).html();
            var template = Handlebars.compile(source);
            layout.html(template(data));
        }
    });

    $('.nice_images_modal').removeClass('active');
    $('.nice_images_fake_bg').removeClass('active');
}