from __future__ import absolute_import

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class NiceImagesConfig(AppConfig):
    name = 'nice_images'
    verbose_name = _('Nice Images')
