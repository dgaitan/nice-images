from django.core.management.base import BaseCommand
from django.conf import settings
from nice_images.models import Image
from PIL import Image as Photo


class Command(BaseCommand):

    def handle(self, *args, **options):

        for i in Image.objects.all():
            path = i.photo.url
            path_list = path.split('.')
            full_path = '{0}{1}'.format(
                settings.MEDIA_ROOT.split('/media')[0], path)

            if 'png' in path_list:
                print('Fixing image: ', i)
                image = Photo.open(full_path)
                full_name = '{0}{1}_compressed.png'.format(
                    settings.MEDIA_ROOT.split('/media')[0], path_list[0])
                image_name = '{}_compressed.png'.format(
                    path_list[0].replace('/media/', '', 1))
                new_image = image.convert('RGBA')
                new_image.save(full_name)
                i.photo = image_name
                i.save()
