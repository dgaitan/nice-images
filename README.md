# NICE IMAGES

Nice Images is a library to can manage easy the images.
This make it re-usable and easy use friendly.

### Requirements
```
sorl-thumbnail==12.4a1
django-autoslug==1.9.3
Pillow==3.3.1
```

### Getting Started

1. Install the app.
2. Add the app to your THIRD_PARTY_APPS:
```
    ...
    'sorl.thumbnail',
    'autoslug',
    'nice_images',
    ...
```
3. Run the migration
```
    $ python manage.py migrate
```
4. Add the urls to your general urls
```
    ...
    url(r'^nice_images/', include('nice_images.urls', namespace='nice_images')),
    ...
```
5. And you are ready to use it!

### Using the Nice Image Field on our models

To can manage easly the images, yo can use the field of Nice Images like the next code:

```
from django.db import models

from nice_images.fields import NiceImageField


class Post(models.Model):
    title = models.CharField()
    slug = models.SlugField()
    thumbnail = NiceImageField(related_name='post_images')
    body = models.TextField()

    class Meta:
        verbose_name = 'Post'
        verbose_name_plural = 'Posts'

    def __str__(self):
        return self.title

```

Now, you can access to it!

```
from myproject.models import Post

post = Post.objects.last()
print(post.thumbnail.photo.url)
out: '/media/library/images/image.png'

```

We can get a thumb too!

```
print(post.thumbnail.thumb_150x150().url)
out: '/media/cache/1b/c5/1bc5b282727e2bee5a95f67bcc21b3b7.png'
```

To can define our thumb sizes, you can add the next constants on your settings

```
THUMBS_SIZES = ['150x150', '300x300', '600x600', etc...]
```

To can change the path to save the images, you define it with the constant `NICE_IMAGES_UPLOAD_TO`

For example:

```
NICE_IMAGES_UPLOAD_TO = 'my_custom_path/'
```

### Using The Nice Gallery in your models

To can manage easly the gallery using the Nice Images, you need to use the `NiceImageGalleryField` like in the code:

```
from django.db import models

from nice_images.fields import NiceImageField, NiceImageGalleryField


class Post(models.Model):
    title = models.CharField()
    slug = models.SlugField()
    thumbnail = NiceImageField(related_name='post_images')
    gallery = NiceImageGalleryField(related_name='post_galleries')
    body = models.TextField()

    class Meta:
        verbose_name = 'Post'
        verbose_name_plural = 'Posts'

    def __str__(self):
        return self.title
```

Later you can consult it:

```
post = Post.objects.last()
post.gallery
out: <Gallery: Your Gallery>
```

Because the Gallery have detail, first you get the info about the gallery. For example:

```
post.gallery.name
out: 'Your Gallery'

post.gallery.slug
out: 'your-gallery'

# calling the images of this gallery
post.gallery.get_images()
out: <Queryset [<GalleryItem: image_one.jpg>, <GalleryItem: image_two.jpg>, <GalleryItem: image_three.jpg>]>

# showing the images
for image in post.gallery.get_images():
    image.url
    # out: /media/library/images/imagen.jpg'
```

### The Nice Library
To have a beatiful library when the user can manage the images easly do you to ability it! To do it, you need import the view that show it and create the url on your url's like in the next code.

```
# urls.py
# import the view of the Library
from nice_images.views import LibraryView

urlpatterns = [
    url(settings.ADMIN_URL + 'nice_images/library/',
        LibraryView.as_view(), name='nice_image_library'),
    
    ... your another urls settings ...
]
```

so, you can go to your url: http://localhost:8000/admin/nice_images/library/ and see the library. You can change the path if do you want, this is not a problem.